import { Connection } from "https://gitlab.com/napalmbrain/websocket-manager/-/raw/main/src/common.ts";
import { Manager } from "https://gitlab.com/napalmbrain/websocket-manager/-/raw/main/src/manager.ts";

export class Server extends Manager {
  constructor() {
    super();
    this.options.keepalive = true;
  }

  protected init(connection: Connection) {
    super.init(connection);
    connection.timeout!.on(() => {
      this.detach(connection);
    });
  }

  protected deinit(connection: Connection) {
    super.deinit(connection);
    for (const action of connection.actions!.values()) {
      action.observable.complete();
    }
  }

  public upgrade(request: Request): Response {
    const { socket, response } = Deno.upgradeWebSocket(request);
    const connection: Connection = {
      socket,
    };
    this.attach(connection);
    return response;
  }

  protected onclose(connection: Connection) {
    super.onclose(connection);
    this.detach(connection);
  }
}
